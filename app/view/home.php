<div id="services">
	<div class="row">
		<h1>OUR SERVICES</h1>
		<div class="svc-images">
			<dl class="col-4 fl">
				<dt> <img src="public/images/content/svc1.png" alt="Services Image"> </dt>
				<dd>
					<p>Land Clearing</p>
					<a href="services#content" class="btn">MORE</a>
				</dd>
			</dl>
			<dl class="col-4 fl">
				<dt> <img src="public/images/content/svc2.png" alt="Services Image"> </dt>
				<dd>
					<p>Landscape Design</p>
					<a href="services#content" class="btn">MORE</a>
				</dd>
			</dl>
			<dl class="col-4 fl">
				<dt> <img src="public/images/content/svc3.png" alt="Services Image"> </dt>
				<dd>
					<p>Stump Grinding</p>
					<a href="services#content" class="btn">MORE</a>
				</dd>
			</dl>
			<div class="clearfix"></div>
		</div>
		<h2>Designing, Building & Maintaining Your Garden</h2>
		<p> <img src="public/images/common/sprite.png" alt="phone icon" class="bg-wcphone"> <span><?php $this->info(["phone","tel"]); ?></span> </p>
	</div>
</div>
<div id="about">
	<div class="aboutTop">
		<div class="row">
			<div class="aboutTopRight col-6 fr">
				<h1>R.B. & Company, INC</h1>
				<h2>LANDSCAPING SERVICES</h2>
				<p>We hope you can find everything you need. R. B. & Company, Inc. is focused on providing high-quality service and customer satisfaction - we will do everything we can to meet your expectations.</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="aboutBot">
		<div class="row">
			<div class="aboutBotLeft col-6 fl">
				<h1>We are a family owned & <br> operated business.</h1>
				<p>With a variety of offerings to choose from, we’re sure you’ll be happy working with us. Look around our website and if you have any comments or questions, please feel free to contact us.</p>
				<a href="about#content" class="btn">READ MORE</a>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<div id="projects">
	<div class="row">
		<h1> <span class="green">RECENT</span> <span class="orange">PROJECTS</span> </h1>
		<div class="container">
			<img src="public/images/content/project1.jpg" alt="Project 1">
			<img src="public/images/content/project2.jpg" alt="Project 2">
			<img src="public/images/content/project3.jpg" alt="Project 3">
		</div>
	</div>
</div>
<div id="testimonials">
	<div class="row">
		<div class="testLeft col-6 fl">
			<img src="public/images/content/testImg.jpg" alt="Home">
		</div>
		<div class="testRight col-6 fl">
			<div class="container">
				<p>
					<span>&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>
					Just wanted to tell you what a wonderful job you are doing with my lawn. Looks very trim and neat and you haven’t broken any sprinkler heads! Keep up the good work!. Keep trimming by the sprinkler heads and weeding. Looks great!
				</p>
				<a href="services#content" class="btn">VIEW MORE</a>
			</div>
		</div>
		<div class="clearfix">

		</div>
	</div>
</div>
